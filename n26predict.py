# Import necessary modules
import pandas as pd
import numpy as np

from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor

from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, train_test_split
from sklearn.metrics import mean_squared_error, explained_variance_score, r2_score

from sklearn.externals import joblib

import matplotlib.pyplot as plt
import seaborn as sns

import sys
from time import strftime, localtime

def main(new_data):

    # Loading data
    test = pd.read_csv(new_data, na_values='NA')
    mcc_group = pd.read_csv('data/mcc_group_definition.csv')
    tran_types = pd.read_csv('data/transaction_types.csv')

    # Look on datasets
    print("{1} read into Dataframe test shape of {0}".format(test.shape, new_data))
    
    # Joining all three tables between each other
    test_full = pd.merge(test, mcc_group, how='left', on='mcc_group')
    test_full = test_full.rename(columns={'explanation': 'expl_mcc'})
    test_full = pd.merge(test_full, tran_types, how='left', left_on='transaction_type', right_on='type')
    test_full = test_full.rename(columns={'explanation': 'expl_trans'})
    test_full = test_full.drop(['type', 'dataset_transaction', 'dataset_user'], axis=1)

    # check that join didn't change number of records
    assert (test.shape[0] == test_full.shape[0])



    # Getting month from transaction date
    test_full['month'] = pd.to_datetime(test_full['transaction_date']).dt.strftime('%Y-%m')

    # Remove spaces from agent variable
    test_full['agent'] = test_full['agent'].replace(' ', '_', regex=True)

    # Create dummy variable for 'transaction_type', 'agent' and 'mcc_group'
    test_full = test_full.join(
        pd.get_dummies(test_full[['transaction_type', 'agent', 'mcc_group']], dummy_na=True, drop_first=True))

    # Check if shape is the same
    diff_list = list(set(['transaction_type_BUB', 'transaction_type_CT', 'transaction_type_DD', 'transaction_type_DR',
              'transaction_type_DT', 'transaction_type_FT', 'transaction_type_PT', 'transaction_type_TUB',
              'agent_Card', 'agent_Partner', 'mcc_group_2.0', 'mcc_group_3.0', 'mcc_group_4.0', 'mcc_group_5.0',
              'mcc_group_6.0', 'mcc_group_7.0', 'mcc_group_8.0', 'mcc_group_9.0', 'mcc_group_10.0', 'mcc_group_11.0',
              'mcc_group_12.0', 'mcc_group_13.0', 'mcc_group_14.0', 'mcc_group_15.0', 'mcc_group_16.0', 'mcc_group_17.0', 'mcc_group_nan']) - set(test_full.columns.values))
    if len(diff_list) > 0:
        for var in diff_list:
            test_full[var] = 0

    test_agg = pd.DataFrame()
    if 'amount_n26_currency' in test_full.columns.values:
        # Dividing amount into In and Out and making out negative
        def target_builder(row):
            target = row['amount_n26_currency']
            if row['transaction_type'] == 'AR':
                target = 0
            elif row['direction'] == 'In':
                target = row['amount_n26_currency']
            elif row['direction'] == 'Out':
                target = -1 * row['amount_n26_currency']
            return target

        test_full['target'] = test_full.apply(target_builder, axis=1)

        # Creation of aggregation by month ad user ID
        test_agg = test_full.groupby(['user_id', 'month']).agg(
            {'target': 'sum', 'transaction_type_BUB': 'sum', 'transaction_type_CT': 'sum',
             'transaction_type_DD': 'sum', 'transaction_type_DR': 'sum', 'transaction_type_DT': 'sum',
             'transaction_type_FT': 'sum', 'transaction_type_PT': 'sum', 'transaction_type_TUB': 'sum',
             'agent_Card': 'sum', 'agent_Partner': 'sum', 'mcc_group_2.0': 'sum', 'mcc_group_3.0': 'sum',
             'mcc_group_4.0': 'sum', 'mcc_group_5.0': 'sum',
             'mcc_group_6.0': 'sum', 'mcc_group_7.0': 'sum', 'mcc_group_8.0': 'sum', 'mcc_group_9.0': 'sum',
             'mcc_group_10.0': 'sum', 'mcc_group_11.0': 'sum', 'mcc_group_12.0': 'sum', 'mcc_group_13.0': 'sum',
             'mcc_group_14.0': 'sum', 'mcc_group_15.0': 'sum', 'mcc_group_16.0': 'sum', 'mcc_group_17.0': 'sum',
             'mcc_group_nan': 'sum'})

    else:
        # Creation of aggregation by month ad user ID
        test_agg = test_full.groupby(['user_id', 'month']).agg(
            {'transaction_type_BUB': 'sum', 'transaction_type_CT': 'sum',
             'transaction_type_DD': 'sum', 'transaction_type_DR': 'sum', 'transaction_type_DT': 'sum',
             'transaction_type_FT': 'sum', 'transaction_type_PT': 'sum', 'transaction_type_TUB': 'sum',
             'agent_Card': 'sum', 'agent_Partner': 'sum', 'mcc_group_2.0': 'sum', 'mcc_group_3.0': 'sum',
             'mcc_group_4.0': 'sum', 'mcc_group_5.0': 'sum',
             'mcc_group_6.0': 'sum', 'mcc_group_7.0': 'sum', 'mcc_group_8.0': 'sum', 'mcc_group_9.0': 'sum',
             'mcc_group_10.0': 'sum', 'mcc_group_11.0': 'sum', 'mcc_group_12.0': 'sum', 'mcc_group_13.0': 'sum',
             'mcc_group_14.0': 'sum', 'mcc_group_15.0': 'sum', 'mcc_group_16.0': 'sum', 'mcc_group_17.0': 'sum',
             'mcc_group_nan': 'sum'})

    if 'target' in test_agg.columns.values:
        X = test_agg.drop('target', axis=1).values
    else:
        X = test_agg.values


    # Load model
    n26_model = joblib.load('n26_model.pkl')

    # Evaluating model on new set
    y_pred = n26_model.predict(X)

    if 'target' in test_agg.columns.values:
        y = test_agg['target'].values
        print("MSE: %.4f" % mean_squared_error(y, y_pred))
        print("Explained variance: %.4f" % explained_variance_score(y, y_pred))
        print("R2: %.4f" % r2_score(y, y_pred))

    test_agg['amount_predicted'] = y_pred
    test_agg.to_csv('{0}_scored.csv'.format(new_data[:-4]))

if __name__ == '__main__':
    print("Started scoring new data for N26 task at {0}".format(strftime("%Y-%m-%d %H:%M:%S", localtime())))
    if len(sys.argv) > 1:
        new_file = sys.argv[1]
        main(new_file)
    else:
        print("No new file")
    print("Ended scoring new data for N26 task at {0}".format(strftime("%Y-%m-%d %H:%M:%S", localtime())))

