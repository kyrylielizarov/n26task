# Import necessary modules
import pandas as pd
import numpy as np

from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor

from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, train_test_split
from sklearn.metrics import mean_squared_error, explained_variance_score, r2_score

from sklearn.externals import joblib

import matplotlib.pyplot as plt
import seaborn as sns

import sys
from time import strftime, localtime

def main():

    # Loading data
    train = pd.read_csv('data/2016-09-19_79351_training.csv', na_values='NA')
    mcc_group = pd.read_csv('data/mcc_group_definition.csv')
    tran_types = pd.read_csv('data/transaction_types.csv')

    # First look on datasets
    print("2016-09-19_79351_training.csv read into Dataframe train shape of {0}".format(train.shape))
    print(train.head())
    print("\n mcc_group_definition.csv read into Dataframe mcc_group shape of {0}".format(mcc_group.shape))
    print(mcc_group.head())
    print("\n transaction_types.csv read into Dataframe tran_types shape of {0}".format(tran_types.shape))
    print(tran_types.head())

    # Look on variables 'dataset_transaction' and 'dataset_user'
    print(train['dataset_transaction'].value_counts())
    print(train['dataset_user'].value_counts())

    # Joining all three tables between each other
    train_full = pd.merge(train, mcc_group, how='left', on='mcc_group')
    train_full = train_full.rename(columns={'explanation': 'expl_mcc'})
    train_full = pd.merge(train_full, tran_types, how='left', left_on='transaction_type', right_on='type')
    train_full = train_full.rename(columns={'explanation': 'expl_trans'})
    train_full = train_full.drop(['type', 'dataset_transaction', 'dataset_user'], axis=1)

    # check that join didn't change number of records
    assert (train.shape[0] == train_full.shape[0])

    # Show information on full dataset
    print(train_full.info())
    print(train_full['transaction_type'].value_counts())
    print(train_full['mcc_group'].value_counts())

    # Dividing amount into In and Out and making out negative
    def target_builder(row):
        target = row['amount_n26_currency']
        if row['transaction_type'] == 'AR':
            target = 0
        elif row['direction'] == 'In':
            target = row['amount_n26_currency']
        elif row['direction'] == 'Out':
            target = -1 * row['amount_n26_currency']
        return target

    train_full['target'] = train_full.apply(target_builder, axis=1)

    # Getting month from transaction date
    train_full['month'] = pd.to_datetime(train_full['transaction_date']).dt.strftime('%Y-%m')

    # Show aggregation of average ant total amount by direction
    print(train_full.groupby(['direction']).agg({'user_id': 'count', 'target': ['sum', 'mean']}))

    # Remove spaces from agent variable
    train_full['agent'] = train_full['agent'].replace(' ', '_', regex=True)

    # Create dummy variable for 'transaction_type', 'agent' and 'mcc_group'
    train_full = train_full.join(
        pd.get_dummies(train_full[['transaction_type', 'agent', 'mcc_group']], dummy_na=True, drop_first=True))

    # Plot summary on data frame with joined dummy variables
    print(train_full.info())

    # Create plot for distribution of average In and Out flow by month
    df = train_full.groupby(['month', 'direction']).agg({'amount_n26_currency': 'mean'})
    df = df.reset_index()

    pos = list(range(len(df['month'].unique())))
    width = 0.25

    fig, ax = plt.subplots(figsize=(10, 5))

    plt.bar(pos, df.loc[df['direction'] == 'In', 'amount_n26_currency'], width, alpha=0.5, color='#F78F1E')
    plt.bar([p + width for p in pos], df.loc[df['direction'] == 'Out', 'amount_n26_currency'], width, alpha=0.5,
            color='#FFC222')

    ax.set_ylabel('Amount, N26 currency')
    ax.set_title('Average by month amount')

    ax.set_xticks([p + 0.5 * width for p in pos])
    ax.set_xticklabels(df['month'])

    plt.xlim(min(pos) - width, max(pos) + width * 2)
    plt.ylim([0, max(df['amount_n26_currency']) + 5])

    plt.legend(['In Flow', 'Out Flow'], loc='upper left')
    plt.grid()
    # uncomment to see plot
    # plt.show()

    # Plot average flows by transaction types
    df = train_full.groupby(['expl_trans', 'direction']).agg({'amount_n26_currency': 'mean'})
    df = df.reset_index()

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(25, 7))

    ax1.bar(df.loc[df['direction'] == 'In', 'expl_trans'], df.loc[df['direction'] == 'In', 'amount_n26_currency'],
            alpha=0.8)
    ax2.bar(df.loc[df['direction'] == 'Out', 'expl_trans'], df.loc[df['direction'] == 'Out', 'amount_n26_currency'],
            alpha=0.8)

    ax1.set_ylabel('Amount, N26 currency')
    ax1.set_title('In flow Average by transaction type')
    ax2.set_title('Out flow Average by transaction type')

    ax1.set_xticklabels(df.loc[df['direction'] == 'In', 'expl_trans'])
    ax2.set_xticklabels(df.loc[df['direction'] == 'Out', 'expl_trans'])

    rects = ax1.patches
    labels = df.loc[df['direction'] == 'In', 'amount_n26_currency'].apply(lambda x: round(x, 2)).values
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax1.text(rect.get_x() + rect.get_width() / 2, height + 1, label, ha='center', va='bottom')

    rects = ax2.patches
    labels = df.loc[df['direction'] == 'Out', 'amount_n26_currency'].apply(lambda x: round(x, 2)).values
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax2.text(rect.get_x() + rect.get_width() / 2, height + 1, label, ha='center', va='bottom')
    # uncomment to see plot
    # plt.show()

    # Plot average flows by agent
    df = train_full.groupby(['agent', 'direction']).agg({'amount_n26_currency': 'mean'})
    df = df.reset_index()

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(25, 7))

    ax1.bar(df.loc[df['direction'] == 'In', 'agent'], df.loc[df['direction'] == 'In', 'amount_n26_currency'], alpha=0.8)
    ax2.bar(df.loc[df['direction'] == 'Out', 'agent'], df.loc[df['direction'] == 'Out', 'amount_n26_currency'],
            alpha=0.8)

    ax1.set_ylabel('Amount, N26 currency')
    ax1.set_title('In flow Average by agent')
    ax2.set_title('Out flow Average by agent')

    ax1.set_xticklabels(df.loc[df['direction'] == 'In', 'agent'])
    ax2.set_xticklabels(df.loc[df['direction'] == 'Out', 'agent'])

    rects = ax1.patches
    labels = df.loc[df['direction'] == 'In', 'amount_n26_currency'].apply(lambda x: round(x, 2)).values
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax1.text(rect.get_x() + rect.get_width() / 2, height + 1, label, ha='center', va='bottom')

    rects = ax2.patches
    labels = df.loc[df['direction'] == 'Out', 'amount_n26_currency'].apply(lambda x: round(x, 2)).values
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax2.text(rect.get_x() + rect.get_width() / 2, height + 1, label, ha='center', va='bottom')
    # uncomment to see plot
    # plt.show()

    # Plot average out flow by MCC group
    df = train_full.groupby(['expl_mcc', 'direction']).agg({'amount_n26_currency': 'mean'})
    print(df)
    df = df.reset_index()

    fig, ax = plt.subplots(figsize=(30, 10))

    ax.bar(df.loc[df['direction'] == 'Out', 'expl_mcc'], df.loc[df['direction'] == 'Out', 'amount_n26_currency'],
           alpha=0.8)

    ax.set_ylabel('Amount, N26 currency')
    ax.set_title('Out flow Average by MCC group')

    ax.set_xticklabels(df.loc[df['direction'] == 'Out', 'expl_mcc'])

    rects = ax.patches
    labels = df.loc[df['direction'] == 'Out', 'amount_n26_currency'].apply(lambda x: round(x, 2)).values
    for rect, label in zip(rects, labels):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width() / 2, height + 1, label, ha='center', va='bottom')

    # uncomment to see plot
    # plt.show()

    # Creation of aggregation by month ad user ID
    train_agg = train_full.groupby(['user_id', 'month']).agg(
        {'target': 'sum', 'transaction_type_BUB': 'sum', 'transaction_type_CT': 'sum',
         'transaction_type_DD': 'sum', 'transaction_type_DR': 'sum', 'transaction_type_DT': 'sum',
         'transaction_type_FT': 'sum', 'transaction_type_PT': 'sum', 'transaction_type_TUB': 'sum',
         'agent_Card': 'sum', 'agent_Partner': 'sum', 'mcc_group_2.0': 'sum', 'mcc_group_3.0': 'sum',
         'mcc_group_4.0': 'sum', 'mcc_group_5.0': 'sum',
         'mcc_group_6.0': 'sum', 'mcc_group_7.0': 'sum', 'mcc_group_8.0': 'sum', 'mcc_group_9.0': 'sum',
         'mcc_group_10.0': 'sum', 'mcc_group_11.0': 'sum', 'mcc_group_12.0': 'sum', 'mcc_group_13.0': 'sum',
         'mcc_group_14.0': 'sum', 'mcc_group_15.0': 'sum', 'mcc_group_16.0': 'sum', 'mcc_group_17.0': 'sum',
         'mcc_group_nan': 'sum'})
    print(train_agg.columns.values)

    # Plot of correlations between variable
    corr = train_agg.corr()
    plt.figure(figsize=(10, 10))
    mask = np.zeros_like(corr)
    mask[np.triu_indices_from(mask)] = True
    with sns.axes_style("white"):
        ax = sns.heatmap(corr, mask=mask, vmax=.3, square=True, cmap='RdYlGn')
    # uncomment to see plot
    # plt.show()

    # Modeling and search for optimap model parameters
    X = train_agg.drop('target', axis=1).values
    y = train_agg['target'].values
    # Create train and test sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=25)
    '''
    parameters = {'n_estimators': [100, 150, 200], 'max_depth': [4, 5, 6], 'min_samples_split': [30, 50, 100],
                  'min_samples_leaf': [5, 10],
                  'learning_rate': [0.1], 'loss': ['ls'], 'subsample': [0.8, 0.9, 1.0]}
    '''

    # Optimal parameters
    parameters = {'learning_rate': [0.1], 'loss': ['ls'], 'max_depth': [5], 'min_samples_leaf': [5], 'min_samples_split': [30],
     'n_estimators': [150], 'subsample': [1.0]}
    n26_model = GradientBoostingRegressor()

    # Tuning parameters using Randomized and Grid search
    # gs_n26_model = RandomizedSearchCV(n26_model, parameters, n_iter=200, n_jobs=-1, cv=4, verbose=2)
    gs_n26_model = GridSearchCV(n26_model, parameters, n_jobs=-1, cv=4, verbose=2)

    gs_n26_model = gs_n26_model.fit(X_train, y_train)

    # Evaluating model on test set
    y_pred = gs_n26_model.predict(X_test)

    print(gs_n26_model.best_params_)

    print("MSE: %.4f" % mean_squared_error(y_test, y_pred))
    print("Explained variance: %.4f" % explained_variance_score(y_test, y_pred))
    print("R2: %.4f" % r2_score(y_test, y_pred))

    # Fit regression model using optimal parameters
    params = gs_n26_model.best_params_
    n26_model_fin = GradientBoostingRegressor(**params)

    n26_model_fin.fit(X_train, y_train)
    mse = mean_squared_error(y_test, n26_model_fin.predict(X_test))
    print("MSE: %.4f" % mean_squared_error(y_test, y_pred))
    print("Explained variance: %.4f" % explained_variance_score(y_test, y_pred))
    print("R2: %.4f" % r2_score(y_test, y_pred))

    # Plot training deviance
    test_score = np.zeros((params['n_estimators'],), dtype=np.float64)

    # compute test set deviance
    for i, y_pred in enumerate(n26_model_fin.staged_predict(X_test)):
        test_score[i] = n26_model_fin.loss_(y_test, y_pred)

    plt.figure(figsize=(7, 7))
    plt.title('Deviance')
    plt.plot(np.arange(params['n_estimators']) + 1, n26_model_fin.train_score_, 'b-',
             label='Training Set Deviance')
    plt.plot(np.arange(params['n_estimators']) + 1, test_score, 'r-',
             label='Test Set Deviance')
    plt.legend(loc='upper right')
    plt.xlabel('Boosting Iterations')
    plt.ylabel('Deviance')
    # uncomment to see plot
    # plt.show()

    # Plot feature importance
    vars = train_agg.columns.values[1:]
    feature_importance = n26_model_fin.feature_importances_
    # make importances relative to max importance
    feature_importance = 100.0 * (feature_importance / feature_importance.max())
    sorted_idx = np.argsort(feature_importance)
    pos = np.arange(sorted_idx.shape[0]) + .5
    plt.figure(figsize=(12, 6))
    plt.barh(pos, feature_importance[sorted_idx], align='center')
    plt.yticks(pos, vars[sorted_idx])
    plt.xlabel('Relative Importance')
    plt.title('Variable Importance')
    # uncomment to see plot
    # plt.show()

    # train model on full data
    params = gs_n26_model.best_params_
    n26_model_all = GradientBoostingRegressor(**params)
    n26_model_all.fit(X, y)

    # save the model to disk
    joblib.dump(n26_model_all, 'n26_model.pkl')
    # uncomment to see All plots
    # plt.show()

if __name__ == '__main__':
    print("Started modeling process for N26 task at {0}".format(strftime("%Y-%m-%d %H:%M:%S", localtime())))
    main()
    print("Ended modeling process for N26 task at {0}".format(strftime("%Y-%m-%d %H:%M:%S", localtime())))

